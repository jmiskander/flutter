import 'package:flutter/material.dart';

import 'product_info.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<Product> _products = [];

  final String _baseUrl= "10.0.2.2:9090";

  late Future<bool> fetchedGames;

  Future<bool> fetchGames () async {
    http.Response response= await http.get(Uri.http(_baseUrl, "/game"));
    List<dynamic> gamesFromSever = json.decode(response.body);
    for (int i=0; i<gamesFromSever.length; i++)
    {
      _products.add(Product(gamesFromSever[i]["_id"],gamesFromSever[i]["image"], gamesFromSever[i]["title"],
          gamesFromSever[i]["description"], int.parse(gamesFromSever[i]["price"].toString()),
          int.parse(gamesFromSever[i]["quantity"].toString())));
    }
    return true;
  }


  @override
  void initState() {
    fetchedGames=fetchGames();
  super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchedGames,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if(snapshot.hasData) {
            return ListView.builder(
              itemCount: _products.length,
              itemBuilder: (BuildContext context,int index) {
                return ProductInfo(_products[index].id,_products[index].image, _products[index].title, _products[index].description,
                    _products[index].price, _products[index].quantity);
              },
            );
          }
          else
            {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
      },
    );
  }
}