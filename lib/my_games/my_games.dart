import 'package:flutter/material.dart';
import 'package:seance2/home/product_info.dart';

import 'my_game_info.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

class MyGames extends StatefulWidget {
  const MyGames({Key? key}) : super(key: key);

  @override
  State<MyGames> createState() => _MyGamesState();
}

class _MyGamesState extends State<MyGames> {
  final List<Product> _products = [];

  final String _baseUrl = "10.0.2.2:9090";

  late Future<bool> fetchedGames;

  Future<bool> fetchGames() async {
    http.Response response =
        await http.get(Uri.http(_baseUrl, "/library/617736fbfae2b4afb1912410"));
    List<dynamic> gamesFromSever = json.decode(response.body);
    for (int i = 0; i < gamesFromSever.length; i++) {
      _products.add(Product(
          gamesFromSever[i]["_id"],
          gamesFromSever[i]["image"],
          gamesFromSever[i]["title"],
          gamesFromSever[i]["description"],
          int.parse(gamesFromSever[i]["price"].toString()),
          int.parse(gamesFromSever[i]["quantity"].toString())));
    }
    return true;
  }

  @override
  void initState() {
    fetchedGames = fetchGames();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchedGames,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
          return GridView.builder(
            itemCount: _products.length,
            itemBuilder: (BuildContext context, int index) {
              return MyGameInfo(_products[index].image, _products[index].title);
            },
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 5,
                mainAxisSpacing: 5,
                mainAxisExtent: 130),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
